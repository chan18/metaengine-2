/*
Alignment of structure `hde32s' is 1 byte (no alignment). Be careful, check settings of your compiler or use headers from this package.

hde32 doesn't check invalid instructions. If instruction is invalid, hde32 will disassemble it using the general disassembly rules.

Fields `opcode' and `len' are filled always, others are optional and depend of instruction. If field's value is zero, then it is not existing.

>> http://www.swansontec.com/sintel.html
    ModR/M byte
        If the instruction requires it, the ModR/M byte comes after the opcode. This byte tells the processor which registers or memory locations to use as the instruction's operands. 

        Code    Assembly        Syntax Meaning
        00      [reg1]          The operand's memory address is in reg1.
        01      [reg1 + byte]   The operand's memory address is reg1 + a byte-sized displacement.
        10      [reg1 + word]   The operand's memory address is reg1 + a word-sized displacement.
        11      reg1            The operand is reg1 itself.

    SIB byte
        When ModR/M contains the correct mod and reg1 combination, a SIB byte follows the ModR/M byte. SIB is an acronym which stands for Scale*Index+Base.

*/




typedef struct {
    uint8_t len;        // length of command
    uint8_t p_rep;      // rep/repz (0xf3) & repnz (0xf2) prefix
    uint8_t p_lock;     // lock prefix: 0xf0
    uint8_t p_seg;      // segment prefix: 0x26,0x2e,0x36,0x3e,0x64,0x65
    uint8_t p_66;       // operand-size override prefix: 0x66
    uint8_t p_67;       // address-size override prefix: 0x67
    uint8_t opcode;     // opcode
    uint8_t opcode2;    // second opcode (if first opcode is 0x0f)
    uint8_t modrm;      // ModR/M byte
    uint8_t modrm_mod;  //   mod field of ModR/M
    uint8_t modrm_reg;  //   reg field of ModR/M
    uint8_t modrm_rm;   //   r/m field of ModR/M
    uint8_t sib;        // SIB byte
    uint8_t sib_scale;  //   scale field of SIB
    uint8_t sib_index;  //   index field of SIB
    uint8_t sib_base;   //   base field of SIB
    uint8_t imm8;       // immediate value imm8
    uint16_t imm16;     // immediate value imm16
    uint32_t imm32;     // immediate value imm32
    uint8_t disp8;      // displacement disp8
    uint16_t disp16;    // displacement disp16
    uint32_t disp32;    // displacement disp32
    uint8_t rel8;       // relative address rel8
    uint16_t rel16;     // relative address rel16
    uint32_t rel32;     // relative address rel32
    uint32_t flags;
} hde32s;