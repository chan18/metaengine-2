#include "test.h"

#include <Windows.h>
#include <stdio.h>
#include "pe_header.h"
#include "util.h"


static void func1()
{
    int i = 5;
    i += 1;
}
static void func2()
{
    int i = 10;
    i -= 10;
}

void test_basic_func_disassemblies()
{
    printf("func1\n");
    dump_function_disassembly((BYTE*)func1);
    printf("\nfunc2\n");
    dump_function_disassembly((BYTE*)func2);
    printf("\nmain\n");
    dump_function_disassembly((BYTE*)test_basic_func_disassemblies);
    printf("\n");
}


void test_pe32_load(void* base)
{
    pe32_t pe;
    if (load_pe32_header(base, &pe))
    {
        printf("dos:         0x%08x\n", (DWORD)pe.dos_header);
        printf("nt:          0x%08x\n", (DWORD)pe.nt_header);
        printf("file:        0x%08x\n", (DWORD)pe.file_header);
        printf("data:        0x%08x\n", (DWORD)pe.data_directory);
        printf("image:       0x%08x\n", (DWORD)pe.image_section);
    }
    else printf("load_pe32_header FAILED\n");
}