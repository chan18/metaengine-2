#include "pe_header.h"


BOOL load_pe32_header(void* b, pe32_t* out)
{
    if (b == NULL || out == NULL) return FALSE;

    out->dos_header = (IMAGE_DOS_HEADER*)b;
    if (out->dos_header->e_magic != IMAGE_DOS_SIGNATURE) return FALSE;

    out->nt_header = (IMAGE_NT_HEADERS32*)((DWORD)out->dos_header + out->dos_header->e_lfanew);
    if (out->nt_header->Signature != IMAGE_NT_SIGNATURE) return FALSE;

    out->file_header = (IMAGE_FILE_HEADER*)(&out->nt_header->FileHeader);
    out->opt_header = (IMAGE_OPTIONAL_HEADER32*)(&out->nt_header->OptionalHeader);

    for (int i = 0; i < IMAGE_NUMBEROF_DIRECTORY_ENTRIES; i++)
        out->data_directory[i] = &out->opt_header->DataDirectory[i];

    for (int i = 0; i < IMAGE_NUMBEROF_IMAGE_SECTIONS; i++)
        out->image_section[i] = (IMAGE_SECTION_HEADER*)
        (i * sizeof(IMAGE_SECTION_HEADER) + (DWORD)SECHDROFFSET32(out->dos_header));

    return TRUE;
}