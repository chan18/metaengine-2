#pragma once

#include <Windows.h>
#include "pe_header.h"


DWORD align_address(DWORD address, DWORD alignment);
BOOL get_text_range(pe32_t* pe, DWORD* out_start, DWORD* out_end);

//void log_section_disassembly(const char* path, BYTE* start, BYTE* end);
void log_section_disassembly(const char* path, BYTE* start, DWORD size);
void dump_function_disassembly(BYTE* fn);
void dump_pe_sections(pe32_t* pe);


//int     WINAPI GetListOfResourceTypes(
//    LPVOID    lpFile,
//    HANDLE    hHeap,
//    char      **pszResTypes)
//{
//    PIMAGE_RESOURCE_DIRECTORY          prdRoot;
//    PIMAGE_RESOURCE_DIRECTORY_ENTRY    prde;
//    char                               *pMem;
//    int                                nCnt, i;
//
//
//    /* Get root directory of resource tree. */
//    if ((prdRoot = PIMAGE_RESOURCE_DIRECTORY)ImageDirectoryOffset
//        (lpFile, IMAGE_DIRECTORY_ENTRY_RESOURCE)) == NULL)
//        return 0;
//
//    /* Allocate enough space from heap to cover all types. */
//    nCnt = prdRoot->NumberOfIdEntries * (MAXRESOURCENAME + 1);
//    *pszResTypes = (char *)HeapAlloc(hHeap,
//        HEAP_ZERO_MEMORY,
//        nCnt);
//    if ((pMem = *pszResTypes) == NULL)
//        return 0;
//
//    /* Set pointer to first resource type entry. */
//    prde = (PIMAGE_RESOURCE_DIRECTORY_ENTRY)((DWORD)prdRoot +
//        sizeof(IMAGE_RESOURCE_DIRECTORY));
//
//    /* Loop through all resource directory entry types. */
//    for (i = 0; i<prdRoot->NumberOfIdEntries; i++)
//    {
//        if (LoadString(hDll, prde->Name, pMem, MAXRESOURCENAME))
//            pMem += strlen(pMem) + 1;
//
//        prde++;
//    }
//
//    return nCnt;
//}