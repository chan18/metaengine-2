#include <stdio.h>
#include "util.h"
#include "hde32.h"
#include "pe_header.h"


DWORD align_address(DWORD address, DWORD alignment)
{
    return address + alignment - (address % alignment);
}


static DWORD get_text_address(pe32_t* pe);

BOOL get_text_range(pe32_t* pe, __out DWORD* start, __out DWORD* end)
{
    if (pe == NULL || start == NULL || end == NULL) return FALSE;

    IMAGE_SECTION_HEADER* p = *pe->image_section;
    while (strncmp((char*)p->Name, ".text", 8) != 0) p++;

    *start = (DWORD)pe->dos_header + p->VirtualAddress;
    *end = align_address(*start + p->Misc.VirtualSize, pe->opt_header->SectionAlignment);

    return TRUE;
}


static void dump_disassembly(BYTE* code, hde32s* ds, int index);

void dump_function_disassembly(BYTE* fn)
{
    hde32s ds;
    BYTE* ip = (BYTE*)fn;
    DWORD index = 0;
    DWORD len = 0;
    do
    {
        len = hde32_disasm(ip, &ds);
        index += len;
        dump_disassembly(ip, &ds, index);
        ip += len;
    }
    while (ip && *ip != 0xCC);
}

void dump_disassembly(BYTE* ip, hde32s* ds, int index)
{
    //if (!ds)
    //{
    //    printf("ds was null\n");
    //    return;
    //}
    //printf("0x%08x:    ", (DWORD)code);
    //for (BYTE* x = code; x < code + ds->len; x++)
    //    printf("%02x  ", *x);
    //printf("\n");

    if (ip && ds)
    {
        printf("0x%08x:    ", (DWORD)ip);
        for (BYTE* x = ip; x < ip + ds->len; x++) printf("%02x ", *x);
        printf("\n");
    }
    else printf("dump disassembly passed invalid arg\n");
}


static void log_disassembly(FILE* f, BYTE* ip, hde32s* ds, int index);

//void log_section_disassembly(const char* path, BYTE* start, BYTE* end)
void log_section_disassembly(const char* path, BYTE* start, DWORD size)
{
    if (path == NULL || start == NULL) return;

    FILE* f = fopen(path, "w");
    if (f)
    {
        fprintf(f, "dumping 0x%08x  -  0x%08x\n\n", (DWORD)start, start + size);

        hde32s ds;
        BYTE* ip = (BYTE*)start;
        DWORD index = 0;
        DWORD len = 0;
        do
        {
            len = hde32_disasm(ip, &ds);
            index += len;
            log_disassembly(f, ip, &ds, index);
            ip += len;
        }
        while (ip && ip < start + size);
        fclose(f);
    }
}

static void log_disassembly(FILE* f, BYTE* ip, hde32s* ds, int index)
{
    if (f && ip && ds)
    {
        fprintf(f, "0x%08x:    ", (DWORD)ip);
        for (BYTE* x = ip; x < ip + ds->len; x++) fprintf(f, "%02x ", *x);
        fprintf(f, "\n");
    }
    else fprintf(f, "log disassembly passed invalid arg\n");
}


void dump_pe_sections(pe32_t* pe)
{
    if (pe)
    {
        const int nsections = pe->file_header->NumberOfSections;
        for (int i = 0; i < nsections; i++)
        {
            IMAGE_SECTION_HEADER* image = pe->image_section[i];
            DWORD s = (DWORD)pe->dos_header + image->VirtualAddress;
            DWORD e = align_address(s + image->Misc.VirtualSize, pe->opt_header->SectionAlignment);

            printf("%s:    0x%08x  -  0x%08x\n", (char*)image->Name, s, e);
            printf("    voffset:       0x%08x\n", image->VirtualAddress);
            printf("    vsize:         0x%08x\n", image->Misc.VirtualSize);
            printf("    roffset:       0x%08x\n", image->PointerToRawData);
            printf("    rsize:         0x%08x\n", image->SizeOfRawData);
            //printf("    preloc:        0x%08x\n", image->PointerToRelocations);
            printf("\n");
        }
    } else printf("dump_pe_sections FAILED\n");
}
