#include <Windows.h>
#include <stdio.h>
#include "hde32.h"
#include "pe_header.h"
#include "util.h"
#include "test.h"

#define LOGPATH "C:\\Users\\brad\\Documents\\Visual Studio 2015\\Projects\\metamorphic\\log\\text_section.txt"
#define RELATIVES_LOG "C:\\Users\\brad\\Documents\\Visual Studio 2015\\Projects\\metamorphic\\log\\relatives.txt"
#define IN_LOG "C:\\Users\\brad\\Documents\\Visual Studio 2015\\Projects\\metamorphic\\log\\in.txt"
#define OUT_LOG "C:\\Users\\brad\\Documents\\Visual Studio 2015\\Projects\\metamorphic\\log\\out.txt"

/*
    HDE32 extensions
        - size 3:    00E31A0C     0F01D0      LGDT EAX        ; Illegal use of register
        - size 2:    hde32 has an issue with jump tables (!)
            00E3227C    FE      ???         ; Unknown command
            00E3227D    1E      PUSH DS


*/

/* 
    Every time the engine loads a new instruction for analysis, the first two members of the structure 
    are filled, and the 3rd member is zeroed for later use. The 3rd and 4th fields will only be filled 
    in case the engine analyzes a branch instruction (JMP/Jcc/CALL), to be used when the relocations 
    will be fixed, after the mutation process is complete.

    F_REL flags indicate the instruction requires fixup
    ignore displacement flags
*/
typedef struct
{
    DWORD input_ip;             // Pointer to Instruction in the Input Buffer
    DWORD output_ip;            // Pointer to Mutated Instruction in Output Buffer
    DWORD offset_new_relative;  // Pointer to offset of New Relative Branch Value
    DWORD new_relative;         // New Relative Value for a Branch Instruction
} mutation_table_t;


/* 
MetaEngine(*inBuf, sizeOfCode, *outBuf); 
    1. When the engine first loads, it will use allocate SizeOfCode*16 bytes using VirtualAlloc for the
    purpose of the above-mentioned table. In the end, theses bytes will be freed using VirtualFree. 
    The virus itself uses internal 'caller' functions (callVirtualAlloc / callVirtualFree), and doesn't 
    call the API's directly. 

    2. .text loop
        for each instruction:
            - create entry in mutation table
            - set input_ip and output_ip 
            - if instruction uses relative address, set offset_new_relative and new_relative

    3. for each entry in mutation table:
            - add input_ip and new_relative
            - find instruction in output buf then patch the relative address using offset_new_relative and new_relative


    4. the new buffer should contain executable code (all relocs fixed, etc).  at this point there are choices.
            1. - allocate buffer to memory mapped process or opened file equal to double the original size + room to compensate for extra code
               - copy header, new text buffer, and rest of code to beginning of allocated space.
               - fix any info in the PE header (section pointers, size, etc)
               - save memory mapped process from new allocation to close OR set file pointer to start at new allocation and end at close.

    GENERAL
        - check hde32 for errors, end on error


*/
void run_meta_engine(BYTE* in, int sz_code, BYTE* out)
{
    if (in == NULL) return;
    mutation_table_t* mute_table = calloc(sz_code * 16, sizeof(mutation_table_t));
    if (mute_table == NULL) return;





    free(mute_table);
}



int main(int argc, char** argv)
{
    pe32_t pe;
    HMODULE base = GetModuleHandleA(NULL);
    if (!load_pe32_header(base, &pe))
    {
        printf("Failed to load PE header\n");
        return 0;
    }

    DWORD text_start = (DWORD)base + pe.nt_header->OptionalHeader.BaseOfCode;
    DWORD text_end = (DWORD)base + pe.nt_header->OptionalHeader.BaseOfCode + pe.nt_header->OptionalHeader.SizeOfCode;
    DWORD text_size = pe.nt_header->OptionalHeader.SizeOfCode;

    printf("base:          0x%08x\n", (DWORD)base);
    printf("BaseOfCode:    0x%08x\n", text_start);
    printf("EndOfCode:     0x%08x\n", text_end);
    printf("SizeOfCode:    0x%08x\n\n", text_size);

    int n_relatives = 0, n_instructions = 0, n_errors = 0;

    mutation_table_t* mutation_table = calloc(text_size * 16, sizeof(mutation_table_t));
    if (mutation_table == NULL) return;
    BYTE* mutated_code = (BYTE*)calloc(text_size * 16, sizeof(BYTE));
    if (mutated_code == NULL) return;
    
    hde32s ds;
    BYTE* in_ip = (BYTE*)text_start;
    BYTE* out_ip = mutated_code;
    DWORD index = 0;
    DWORD len = 0;

    do
    {
        len = hde32_disasm(in_ip, &ds);
        index += len;

        for (BYTE* x = in_ip; x < in_ip + len; x++)
        {
            *out_ip = *x;
        }

        if (ds.flags & F_ERROR)
        {
            //fprintf(f, "error:     0x%08x    base + 0x%08x\n", (DWORD)ip, (DWORD)ip - (DWORD)base);
            printf("error:     0x%08x    base + 0x%08x\n", (DWORD)in_ip, (DWORD)in_ip - (DWORD)base);
            printf("  len:     %d\n", ds.len);
            n_errors++;
        }

        //
        // if instruction uses relative address, set offset_new_relative and new_relative
        //
        //if (ds.flags & F_REL8)
        //{
        //    //fprintf(f, "rel8:   0x%08x    base + 0x%08x\n", (DWORD)in_ip, (DWORD)in_ip - (DWORD)base);
        //    n_relatives++;
        //}
        //if (ds.flags & F_REL16)
        //{
        //    //fprintf(f, "rel16:  0x%08x    base + 0x%08x\n", (DWORD)in_ip, (DWORD)in_ip - (DWORD)base);
        //    n_relatives++;
        //}
        //if (ds.flags & F_REL32)
        //{
        //    //fprintf(f, "rel32:  0x%08x    base + 0x%08x\n", (DWORD)in_ip, (DWORD)in_ip - (DWORD)base);
        //    n_relatives++;
        //}

        n_instructions++;
        in_ip += len;
        out_ip += len;
    }
    while (in_ip < (BYTE*)text_end);

    log_section_disassembly(IN_LOG, (BYTE*)text_start, text_size);
    log_section_disassembly(OUT_LOG, mutated_code, text_size);

    free(mutation_table);
    free(mutated_code);

    printf("\n# instructions:  %d    0x%x\n", n_instructions, n_instructions);
    printf("# errors:        %d\n", n_errors);
    printf("# relatives:     %d\n", n_relatives);

    getchar();
    return 0;
}