#pragma once

#include <Windows.h>

// this should be somewhere in windows.h (?)
#define SIZE_OF_NT_SIGNATURE32 sizeof(DWORD)

// Offset to PE file signature
#define NTSIGNATURE(a) ((LPVOID)((BYTE*)a + ((PIMAGE_DOS_HEADER)a)->e_lfanew))

// Section headers are immediately after PE optional header ( modified ).
#define SECHDROFFSET32(a) ((LPVOID)((BYTE*)a                  +  \
                          ((PIMAGE_DOS_HEADER)a)->e_lfanew    +  \
                            SIZE_OF_NT_SIGNATURE32            +  \
                            sizeof (IMAGE_FILE_HEADER)        +  \
                            sizeof (IMAGE_OPTIONAL_HEADER)))

#define IMAGE_NUMBEROF_IMAGE_SECTIONS 5


// 3.24.2016  remove base member, use dos header in place
typedef struct
{
    IMAGE_DOS_HEADER* dos_header;
    IMAGE_NT_HEADERS32* nt_header;
    IMAGE_FILE_HEADER* file_header;
    IMAGE_OPTIONAL_HEADER32* opt_header;
    IMAGE_DATA_DIRECTORY* data_directory[IMAGE_NUMBEROF_DIRECTORY_ENTRIES];
    IMAGE_SECTION_HEADER* image_section[IMAGE_NUMBEROF_IMAGE_SECTIONS];
} pe32_t;


BOOL load_pe32_header(void* b, pe32_t* out);
